xprogtitle = 'PPS v4 Sync Master'
xprogini = 'pps4-sync-master.ini'
xprogrunningnow = 'sync-master-running.now'
xprog = 'pps4-sync-master.exe'
uinitials = "SA"

MODI WINDOW SCREEN TITLE xprogtitle
_screen.Height = 455
_screen.Width = 955
_screen.MaxButton = .f.
_screen.MinButton = .t.
_screen.BorderStyle = 0
_screen.WindowState = 0
_screen.AutoCenter = .t.

*SET RESOURCE off
CLOSE ALL
SET STRICTDATE TO 0
SET ESCAPE OFF
set talk off
set status bar off
set SYSMENU off

modi window screen color rgb(128,128,128,255,255,255),rgb(128,128,128,255,255,255),rgb(128,128,128,255,255,255),rgb(128,128,128,255,255,255),rgb(128,128,128,255,255,255),rgb(128,128,128,255,255,255),rgb(128,128,128,255,255,255),rgb(128,128,128,255,255,255),rgb(128,128,128,255,255,255),rgb(128,128,128,255,255,255) font 'Arial',8 style 'B'
modi window screen color rgb(128,128,128,255,255,255)
clear

set memowidth to 254
SET DELETED ON
SET EXCLUSIVE OFF
*!*	SET STATUS OFF
*!*	SET STATUS BAR OFF
SET BELL OFF
SET CONFIRM ON
SET SAFETY OFF
set sysformats on
set date long
SET DATE BRITISH
SET CENTURY ON
set century to 19 rollover 50
SET EXACT OFF
SET TALK OFF
SET REPROCESS TO AUTOMATIC

SET PROCEDURE TO syncmasterprocs, smsqlprocs ADDITIVE

IF !file('graphics\r.ico')
	IF FILE('update\r.ico')
		lerror = .f.
		xolderror = ON('error')
		ON ERROR lerror=.t.
		COPY FILE "update\r.ico" TO "graphics\r.ico"
		ON ERROR &xolderror
	endif
ENDIF
IF !FILE('graphics\r.ico')
	=okbox('There is a file missing from the Database\graphics folder: R.ICO  Please locate this file and copy it into the DATABASE folder. This must be done before '+xprogtitle+' can run.')
	RETURN
ENDIF

MODI WINDOW SCREEN ICON FILE LOCFILE('graphics\r.ico')

PUBLIC oDynaZip as DZACTXLib.dzactxctrl, oHandler, oUnHandler , lBound, lbound2
PUBLIC gusershutdown, omainform

oDynaZip = CREATEOBJECT("dzactxctrl.dzactxctrl.1")
oHandler = CREATEOBJECT("DzHandler")

lBound = EVENTHANDLER(oDynaZip, oHandler)

oDynaUnZip = CREATEOBJECT("duzactxctrl.duzactxctrl.1")
oUnHandler = CREATEOBJECT("DuzHandler")

lBound2 = EVENTHANDLER(oDynaUnZip, oUnHandler)
*    MainZipForm = thisform
cms = ""
ucms = ''						
gmemo1 = ''
gmemo2 = ''
WITH oDynaZip

	.ZipFile = ''
	.ItemList = 'PPSMemoData'
	.MajorStatusFlag = .f.
	.MinorStatusFlag = .f.
	.quietflag = .t.		
	.AllQuiet = .T.
	.MessageCallBackFlag = .T.
	.UseEncodedBinary = .t.
	UseEncodedBinary = .t.
	.encryptcode = 'FIDJEUNAKJFHASKSLCMFRHAKDKDFJFJM'
ENDWITH
WITH oDynaUnZip

	.ZipFile = ''
	.MajorStatusFlag = .f.
	.MinorStatusFlag = .f.
	.quietflag = .t.		
	.AllQuiet = .T.
	.MessageCallBackFlag = .T.
	.UseEncodedBinary = .t.
	UseEncodedBinary = .t.
	.decryptcode = 'FIDJEUNAKJFHASKSLCMFRHAKDKDFJFJM'
ENDWITH

homedir = SYS(5)+SYS(2003)
currentdatafolder = SYS(5)+SYS(2003)

xcheckstring = ''
xcheckstring = xcheckstring + CHR(0)+CHR(1)+CHR(2)+CHR(3)+CHR(4)+CHR(5)
xcheckstring = xcheckstring + CHR(6)+CHR(7)+CHR(8)+CHR(9)+CHR(11)
xcheckstring = xcheckstring + CHR(12)+CHR(14)+CHR(15)
xcheckstring = xcheckstring + CHR(16)+CHR(17)+CHR(18)+CHR(19)+CHR(20)
xcheckstring = xcheckstring + CHR(21)+CHR(22)+CHR(23)+CHR(24)+CHR(25)
xcheckstring = xcheckstring + CHR(26)+CHR(27)+CHR(28)+CHR(29)+CHR(30)+CHR(31)
*check the registration file
PUBLIC appsfamily(1)
appsfamily(1)=''
M.XPRODUCTNAME = 'PPS'
M.XPRODUCTID = 'PPS4705913'
m.username = ''
m.company = ''
m.serialno1 = ''
m.glicences = 1
developer = .f.
uinitials='SERVER'

IF !DIRECTORY('temp')
	MD temp
endif
if file(xprogrunningnow)
	on error ??
	erase &xprogrunningnow
	on error
endif
if fcreate('sync-master-running.now') < 0
	okbox(xprogtitle+' is already running on this PC.  You cannot run two versions of '+xprogtitle+'.')
	return
ENDIF

xversion = '4'
if file('..\foxtools.fll') OR FILE('foxtools.fll')
	xolderror = ON('error')
	lerror = .f.
	ON ERROR lerror = .t.
	IF FILE('foxtools.fll')
		SET LIBRARY TO foxtools.fll
	else
		set library to ..\foxtools.fll additive
	endif
ENDIF

MODI WINDOW SCREEN TITLE xprogtitle

set excl off
set safety off
ON SHUTDOWN shutdown()
IF !DIRECTORY('temp')
	MD temp
endif
IF !DIRECTORY('log')
	MD log
ENDIF

PUBLIC dropdead
dropdead = .f.
DO WHILE !dropdead
	ON ERROR 
	SET PATH TO livedata\data;temp
	do form mainform.scx NAME omainform
	read events
ENDDO

PROCEDURE shutdown
PARAMETERS xsilent, xrelaunch

&& turn sync timer off while the user ponders
IF TYPE('omainform')=='O' AND !ISNULL(omainform)
	xoldtimer = omainform.maintimer.interval
	omainform.maintimer.interval = 0
	xolderrtimer = omainform.timErrorChecker.interval
	omainform.timErrorChecker.interval = 0
endif

IF !xsilent AND !yesnobox('Are you sure you want to close ' + xprogtitle + '?')
	IF TYPE('omainform')=='O' AND !ISNULL(omainform)
		omainform.maintimer.interval = xoldtimer
		omainform.timErrorChecker.interval = xolderrtimer
	endif
	xshutdown = .f.
	RETURN .f.
ENDIF

IF TYPE('xprogtitle')=='U'
	xprogtitle = 'This program'
endif

dropdead = .t.
ON shutdown
RELEASE ALL EXCEPT xrelaunch
CLOSE ALL
CLEAR events
if file(xprogrunningnow)
	on error ??
	erase "xprogrunningnow"
	on error
ENDIF
IF xrelaunch
	xtimer = SECONDS()
	DO WHILE FILE("xprogrunningnow")
		INKEY(1)
		IF SECONDS()-xtimer > 5
			EXIT
		endif
	ENDDO
	! /n pps4-sync-master.exe relaunch
ENDIF

PROCEDURE shutdownhold

WAIT WINDOW "Please wait, I'm busy!" nowait

gusershutdown = .t.

PROCEDURE shutdownpoll
&& turn timer off while the user ponders
IF TYPE('omainform')=='O' AND !ISNULL(omainform)
	xoldtimer = omainform.maintimer.interval
	omainform.maintimer.interval = 0
	xolderrtimer = omainform.timErrorChecker.interval
	omainform.timErrorChecker.interval = 0
endif

IF yesnobox(xprogtitle + ' is processing, shutting down now may cause errors. Are you sure you want to shutdown?')
	IF TYPE('omainform')=='O' AND !ISNULL(omainform)
		omainform.updatelog('SHUTDOWN',xprogtitle + ' was shut down during processing.')
	endif
	
	DO shutdown WITH .t.
	RELEASE omainform
ELSE
	xshutdown = .f.
	IF TYPE('omainform')=='O' AND !ISNULL(omainform)
		omainform.maintimer.interval = xoldtimer
		omainform.timErrorChecker.interval = xolderrtimer
	endif
ENDIF