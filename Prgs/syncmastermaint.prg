MODI WINDOW SCREEN TITLE 'PPS v4 Synchronisation Server'

*SET RESOURCE off
CLOSE ALL
SET STRICTDATE TO 0
set talk off
modi window screen color rgb(128,128,128,255,255,255),rgb(128,128,128,255,255,255),rgb(128,128,128,255,255,255),rgb(128,128,128,255,255,255),rgb(128,128,128,255,255,255),rgb(128,128,128,255,255,255),rgb(128,128,128,255,255,255),rgb(128,128,128,255,255,255),rgb(128,128,128,255,255,255),rgb(128,128,128,255,255,255) font 'Arial',8 style 'B'
modi window screen color rgb(128,128,128,255,255,255)
clear

set memowidth to 254
SET DELETED ON
SET EXCLUSIVE OFF
SET STATUS OFF
SET STATUS BAR OFF
SET BELL OFF
SET CONFIRM ON
SET SAFETY OFF
set sysformats on
set date long
SET DATE BRITISH
SET CENTURY ON
set century to 19 rollover 50
SET EXACT OFF
SET TALK OFF
SET REPROCESS TO AUTOMATIC

*-- DECLARE DLL statements for reading/writing to private INI files
DECLARE INTEGER GetPrivateProfileString IN Win32API  AS GetPrivStr ;
	String cSection, String cKey, String cDefault, String @cBuffer, ;
	Integer nBufferSize, String cINIFile

DECLARE INTEGER WritePrivateProfileString IN Win32API AS WritePrivStr ;
	String cSection, String cKey, String cValue, String cINIFile

SET PROCEDURE to syncserverprocs


IF !FILE('r.ico') AND FILE('..\r.ico')
	COPY FILE "..\r.ico" TO "r.ico"
ENDIF
IF !FILE('r.ico')
	=okbox('There is a file missing from the PPS-Database folder: R.ICO  Please locate this file and copy it into the PPS-DATABASE folder. This must be done before PPS sync Server can run.')
	RETURN
endif
MODI WINDOW SCREEN ICON FILE LOCFILE('R.ICO')

PUBLIC oDynaZip as DZACTXLib.dzactxctrl, oHandler, oUnHandler , lBound, lbound2

oDynaZip = CREATEOBJECT("dzactxctrl.dzactxctrl.1")
oHandler = CREATEOBJECT("DzHandler")

lBound = EVENTHANDLER(oDynaZip, oHandler)

oDynaUnZip = CREATEOBJECT("duzactxctrl.duzactxctrl.1")
oUnHandler = CREATEOBJECT("DuzHandler")

lBound2 = EVENTHANDLER(oDynaUnZip, oUnHandler)
*    MainZipForm = thisform
cms = ""
ucms = ''						
gmemo1 = ''
gmemo2 = ''
WITH oDynaZip

	.ZipFile = ''
	.ItemList = 'PPSMemoData'
	.MajorStatusFlag = .f.
	.MinorStatusFlag = .f.
	.quietflag = .t.		
	.AllQuiet = .T.
	.MessageCallBackFlag = .T.
	.UseEncodedBinary = .t.
	UseEncodedBinary = .t.
	.encryptcode = 'FIDJEUNAKJFHASKSLCMFRHAKDKDFJFJM'
ENDWITH
WITH oDynaUnZip

	.ZipFile = ''
	.MajorStatusFlag = .f.
	.MinorStatusFlag = .f.
	.quietflag = .t.		
	.AllQuiet = .T.
	.MessageCallBackFlag = .T.
	.UseEncodedBinary = .t.
	UseEncodedBinary = .t.
	.decryptcode = 'FIDJEUNAKJFHASKSLCMFRHAKDKDFJFJM'
ENDWITH

IF !DIRECTORY('temp')
	MD temp
endif
if file('server-running.now')
	on error ??
	erase server-running.now
	on error
endif
if fcreate('server-running.now') < 0
	okbox('PPS v4 Sync is already running on this PC.  You cannot run two versions of PPS v4 Sync.')
	return
endif
xversion = '4'
if file('..\foxtools.fll')
	xolderror = ON('error')
	lerror = .f.
	ON ERROR lerror = .t.
	set library to ..\foxtools.fll additive
	*check the versions of the various main exe files in the system
	IF !lerror
		DIMENSION averarray(12)
		ntheval = GetFileVersion('pps4-sync-server.exe',@averarray)
		IF ntheval = 0
			IF NOT EMPTY(averarray(4))
				xversion = alltrim(averarray(4))
			ENDIF
		ENDIF
	ENDIF
	ON ERROR &xolderror
endif
MODI WINDOW SCREEN TITLE 'PPS Synchronisation Server - v'+xversion

homedir = FULLPATH(CURDIR())
set excl off
set safety off
ON SHUTDOWN shutdown()
IF !DIRECTORY('temp')
	MD temp
endif
IF !DIRECTORY('log')
	MD log
endif
PUBLIC dropdead
dropdead = .f.
DO WHILE !dropdead
	ON ERROR 
	SET PATH TO livedata\data;temp
	do form syncservermaint.scx
	read events
ENDDO


PROCEDURE shutdown
IF !yesnobox('PPS Sync Server should not normally be Closed. Are you sure you want to close PPS Sync Server?')
	RETURN
endif
dropdead = .t.
ON shutdown
RELEASE ALL 
CLOSE ALL
CLEAR events
if file(homedir+'\server-running.now')
	on error ??
	erase "&homedir\server-running.now"
	on error
endif
