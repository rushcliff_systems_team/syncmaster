FUNCTION uniqueid
LOCAL retval
retval = m.serialno1+DTOS(DATE())+RIGHT('00000000'+ALLTRIM(STR(SECONDS()*1000)),8)
DO WHILE retval=m.serialno1+DTOS(DATE())+RIGHT('00000000'+ALLTRIM(STR(SECONDS()*1000)),8)
ENDDO
RETURN retval

function OKbox
parameter xmessage
=messagebox(xmessage,48,'Private Practice Software')
*dboxmess=message
*do shriek.spr
return

********************************************

FUNCTION okcancbox
parameter xmessage, xTimeout
IF EMPTY(xTimeout)
	xTimeout = -1
ENDIF
xreturn = messagebox(xmessage,33+256,'Private Practice Software',xTimeout)
*dboxmess=message
*do okcanc.spr
do case
case xreturn = 1
	return .t.
case xreturn = 2
	return .f.
other 
	return .f.
endcase

FUNCTION yesnobox
parameter xmessage
xreturn = messagebox(xmessage,36+256,'Private Practice Software')
do case
case xreturn = 6
	return .t.
case xreturn = 7
	return .f.
other 
	return .f.
endcase

FUNCTION yesnocancbox
parameter xmessage
xreturn = messagebox(xmessage,35+512,'Private Practice Software')
*dboxmess=message
*do okcanc.spr
do case
case xreturn = 6
	return 1
case xreturn = 7
	return 2
other 
	return 3
endcase

function stopbox
parameter xmessage, timeout
if pcount() = 1
	timeout = 0
ELSE
	timeout = timeout * 1000
endif
=messagebox(xmessage,16,'Private Practice Software', timeout)
return

function infobox
parameter xmessage,xtimeout
IF EMPTY(xtimeout)
	xtimeout = 0
endif
=messagebox(xmessage,64,'Private Practice Software',xtimeout)
return

FUNCTION checkinf2
PARAMETER M.XXFILE, M.XXPRODUCTNAME, M.XXPRODUCTID,xxdemo

xfailedmsg = 'An invalid '+alltrim(m.xxfile)+' file has been detected. '+xprogtitle+' will not run until PPS has been registered.'

*set up interlacing strings
m.xiserialno = 'ALVPOD'
m.xfilename = ' DSUJCID'
m.xusername = 'KDFSUCJHKSNFHWFHDIOKHDGZSDFVJXDFSFCVGFRTWLOZXCPAOS'
m.INTERLACE2 = 'EKSLDI'

fhandle = fopen(m.xxfile)
*fail to open (000)
if fhandle < 0
	IF !xxdemo
		=stopbox(alltrim(m.xxfile)+' could not be accessed. '+xprogtitle+' will not run until PPS has been registered.')
	endif
	return .f.
endif
filesize = fseek(fhandle,0,2)
=fseek(fhandle,0)
filestring = fread(fhandle,filesize)
=fclose(fhandle)

*infdcrypion
*infdcrypion
IF filesize>300
	fstring = accessdecrypt(filestring)
else
	fstring = infdcryp(filestring)
ENDIF

*check the initial checksum (001)
m.xxchecksum = VAL(SUBSTR(fstring,1,10))
xxchecksum2 = 0
FOR LOOP = 11 TO LEN(fstring)
	xxchecksum2 = xxchecksum2 + ASC(SUBSTR(fstring,loop,1))
ENDFOR

IF xxchecksum!=xxchecksum2
	IF !xxdemo
		=stopbox(xfailedmsg + '(001)')
	endif
	return .f.
endif

*now check the fields from fstring (002)
if m.xxproductid != substr(fstring,11,10)
	IF !xxdemo
		=stopbox(xfailedmsg + '(002)')
	endif
	return .f.
endif
	

*reduce fstring down
fstring = substr(fstring,21)

*serialno check (003)
m.serialno1 = substr(fstring,1,1)+substr(fstring,3,1)+substr(fstring,5,1)+substr(fstring,7,1)+substr(fstring,9,1)+substr(fstring,11,1)
m.xinterlace1 = substr(fstring,2,1)+substr(fstring,4,1)+substr(fstring,6,1)+substr(fstring,8,1)+substr(fstring,10,1)+substr(fstring,12,1)
if m.xinterlace1 != m.xiserialno
	IF !xxdemo
		=stopbox(xfailedmsg + '(003)')
	endif
	return .f.
endif


*reduce fstring down
fstring = substr(fstring,13)

*EXE file name (004)
m.exe = substr(fstring,1,9)
m.exe = left(substr(m.exe,1,at('&',m.exe)-1)+space(8),8)
if m.exe != m.xxproductname
	IF !xxdemo
		=stopbox(xfailedmsg + '(004)')
	endif
	return .f.
endif

*reduce fstring down
fstring = substr(fstring,10)

*check the user name,company and interlace string (005)
m.ustr = ''
m.cstr = ''
m.xstr = ''
for loop = 1 to 150 step 3
	m.ustr = m.ustr+substr(fstring,loop,1)
	m.cstr = m.cstr+substr(fstring,loop+1,1)
	m.xstr = m.xstr+substr(fstring,loop+2,1)
next
if m.xusername != m.xstr
	IF !xxdemo
		=stopbox(xfailedmsg + '(005)')
	endif
	return .f.
endif

*reduce fstring down
fstring = substr(fstring,151)	


*check the second serialno (006)
m.serialno2 = substr(fstring,1,1)+substr(fstring,3,1)+substr(fstring,5,1)+substr(fstring,7,1)+substr(fstring,9,1)+substr(fstring,11,1)
if m.serialno2 != m.serialno1
	IF !xxdemo
		=stopbox(xfailedmsg + '(006)')
	endif
	return .f.
endif

*second checksum (007)
m.checksum = substr(fstring,2,1)+substr(fstring,4,1)+substr(fstring,6,1)+substr(fstring,8,1)+substr(fstring,10,1)+substr(fstring,12,1)

if substr(m.checksum,1,3) != substr(m.checksum,4,3)
	IF !xxdemo
		=stopbox(xfailedmsg + '(007)')
	endif
	return .f.
endif

*serial number/licences checksum
*1 (008)
m.checksum1 = chr(64+int((((val(substr(m.serialno1,1,2))+1)*25)/100)+.99999999))
*2 (009)
m.checksum2 = chr(64+int((((val(substr(m.serialno1,3,2))+1)*25)/100)+.99999999))
*3 (010)
m.checksum3 = chr(64+int((((val(substr(m.serialno1,5,2))+1)*25)/100)+.99999999))

if substr(m.checksum,1,1) != m.checksum1
	IF !xxdemo
		=stopbox(xfailedmsg + '(008)')
	endif
	return .f.
endif
if substr(m.checksum,2,1) != m.checksum2
	IF !xxdemo
		=stopbox(xfailedmsg + '(009)')
	endif
	return .f.
endif
if substr(m.checksum,3,1) != m.checksum3
	IF !xxdemo
		=stopbox(xfailedmsg + '(010)')
	endif
	return .f.
endif

m.fstring = SUBSTR(m.fstring,13)
*now dig out the number of licences (011)

xxlic1 = SUBSTR(fstring,1,7)+SUBSTR(fstring,9,1)+SUBSTR(fstring,11,1)+SUBSTR(fstring,13,1)+SUBSTR(fstring,15,1)
xxlic2 = SUBSTR(fstring,8,1)+SUBSTR(fstring,10,1)+SUBSTR(fstring,12,1)+SUBSTR(fstring,14,1)

IF !ROUND(VAL(xxlic1)*VAL(xxlic2),0) = VAL(m.serialno1)
	*invalid
	IF !xxdemo
		=stopbox(xfailedmsg + '(011)')
	endif
	return .f.
endif

*
fstring = SUBSTR(fstring,16)

*Extra Modules
*clinical Notes (012)
DO case
case SUBSTR(fstring,1,1) = '1'
	m.gregclinical = .t.
CASE SUBSTR(fstring,1,1) = '0'
	m.gregclinical = .f.
OTHERWISE
	IF !xxdemo
		=stopbox(xfailedmsg + '(012)')
	endif
	return .f.
endcase
*custom forms (013)
DO case
case SUBSTR(fstring,2,1) = '1'
	m.gregforms = .t.
CASE SUBSTR(fstring,2,1) = '0'
	m.gregforms = .f.
OTHERWISE
	IF !xxdemo
		=stopbox(xfailedmsg + '(013)')
	endif
	return .f.
endcase
*expense (014)
DO case
case SUBSTR(fstring,3,1) = '1'
	m.gregexpense = .t.
CASE SUBSTR(fstring,3,1) = '0'
	m.gregexpense = .f.
OTHERWISE
	IF !xxdemo
		=stopbox(xfailedmsg + '(014)')
	endif
	return .f.
endcase

*expiry date (015)
m.expiry = CTOD(SUBSTR(fstring,19,2)+'/'+SUBSTR(fstring,17,2)+'/'+SUBSTR(fstring,13,4))
IF !xxdemo
	IF (!VAL(SUBSTR(fstring,13,4)) > 2000 OR !BETWEEN(VAL(SUBSTR(fstring,17,2)),1,12) OR !BETWEEN(VAL(SUBSTR(fstring,19,2)),1,31)) AND !EMPTY(SUBSTR(fstring,13,8))
		=stopbox(xfailedmsg + '(015)')
		return .f.
	endif
	IF !EMPTY(m.expiry) AND m.expiry < DATE()
		=stopbox('Your PPS Licence has expired. Please contact your PPS supplier to renew your licence. '+xprogtitle+' will not run until PPS has been registered.')
		RETURN .f.
	endif
	IF !EMPTY(m.expiry) AND m.expiry< DATE()+14
*		=okbox('Your PPS Licence will expire in '+ALLTRIM(STR(DATE()-m.expiry))+' days. Please contact your PPS supplier to renew your licence.') 
	endif
ENDIF

m.suppexpiry = CTOD(SUBSTR(fstring,11,2)+'/'+SUBSTR(fstring,9,2)+'/'+SUBSTR(fstring,5,4))
IF LEN(fstring) < 23
	m.gregjayex = .f.
ELSE
	*Jayex
	DO case
	case SUBSTR(fstring,21,1) = '1'
		m.gregjayex = .t.
	otherwise
		m.gregjayex = .f.
	endcase
ENDIF
IF LEN(fstring) < 24
	m.gregwalkin = .f.
ELSE
	*Walkin appointments
	DO case
	case SUBSTR(fstring,22,1) = '1'
		m.gregwalkin = .t.
	otherwise
		m.gregwalkin = .f.
	endcase
ENDIF

m.username = m.ustr

return .t.


***************************************
*sql audit
PROCEDURE dosqlupdateaudit
PARAMETERS XACTION,xclientno,XTABLE,xindexkey,xdetails,xprocessed

IF EMPTY(xclientno)
	IF TYPE(xtable+'.clientno')='U'
		xclientno = ''
	ELSE
		xclientno = &xtable..clientno
	endif
endif
IF EMPTY(xdetails)
	xdetails = ''
endif
IF EMPTY(xprocessed)
	xprocessed = {//::}
endif  
XTABLE = UPPER(ALLTRIM(XTABLE))
DO case
CASE xaction = 'INSERT'
	IF EMPTY(xindexkey)
		xindexkey = ''
	endif
	*get the record set into xml
	xaudstring = ''	
	CURSORtoxml(xtable,'xaudstring',1,1,1)
	xaudstring = zipencrypt(xaudstring)
	INSERT INTO audit (clientno,table,action,date,time,user,details,indexkey,fields,processed) VALUES (xclientno,xtable,xaction,DATE(),TIME(),uinitials,xaudstring,xindexkey,xdetails,xprocessed)
	IF USED('audit')
		USE IN audit
	endif
CASE xaction = 'UPDATE'
	*get the record set into xml
	xaudstring = ''	
	CURSORtoxml(xtable,'xaudstring',1,1,1)
	xaudstring = zipencrypt(xaudstring)
	INSERT INTO audit (clientno,table,action,date,time,user,details,indexkey,fields,processed) VALUES (xclientno,xtable,xaction,DATE(),TIME(),uinitials,xaudstring,xindexkey,xdetails,xprocessed)
	IF USED('audit')
		USE IN audit
	endif
CASE xaction = 'DELETE'
	xaudstring = ''	
	CURSORtoxml(xtable,'xaudstring',1,1,1)
	xaudstring = zipencrypt(xaudstring)
	INSERT INTO audit (clientno,table,action,date,time,user,details,indexkey,fields,processed) VALUES (xclientno,xtable,xaction,DATE(),TIME(),uinitials,xaudstring,xindexkey,xdetails,xprocessed)
	IF USED('audit')
		USE IN audit
	endif
CASE xaction = 'ACTION'
	INSERT INTO audit (clientno,table,action,date,time,user,indexkey,fields,processed) VALUES (xclientno,xtable,xaction,DATE(),TIME(),uinitials,xindexkey,xdetails,xprocessed)
	IF USED('audit')
		USE IN audit
	endif
endcase
RETURN


function opendbf
parameter xfile,xindex,xexclusive,xalias
if pcount()<4
	xalias=xfile
endif
if pcount()<3
	xexclusive=.f.
endif
if pcount()<2
	xindex=''
endif
if pcount()<1
	return .f.
endif
errortimes = 0
xolderror = ON('error')
xlerror = .f.
ON ERROR xlerror = .t.
m.talkstat = set('TALK')
set talk off
if used(xalias)
	select (xalias)
	if xexclusive
		use (xfile) exclusive alias (xalias)
		IF !USED(XALIAS)
			on error &xolderror
			RETURN .F.
		ENDIF
	endif
	wasused=.t.
else
	select 0
	if xexclusive
		use (xfile) again exclusive alias (xalias)
		IF !USED(XALIAS)
			on error &xolderror
			RETURN .F.
		ENDIF
	else
		use (xfile) again shared alias (xalias)
		IF !USED(XALIAS)
			on error &xolderror
			RETURN .F.
		ENDIF
	endif
	wasused=.f.
endif
if !empty(xindex)
	set order to (xindex)
else
	IF !WASUSED
		set order to
	ENDIF
endif
if !wasused
	go top
endif
set talk &talkstat
on error &xolderror
return .t.

function closedbf
parameter xfile
if used(xfile)
	select (xfile)
	use
endif
xfile=upper(xfile)
return

PROCEDURE opencursor
PARAMETERS xcursor
IF !USED(xcursor)
	IF !FILE('temp\'+xcursor+'.dbf')
		createcursor(SUBSTR(xcursor,2))
	ELSE
		SELECT 0
		USE temp\&xcursor excl
	endif
ELSE
	SELECT (xcursor)
endif

PROCEDURE createcursor
PARAMETERS xtable
IF USED('c'+xtable)
	SELECT c&xtable
	USE
endif
opendbf(xtable)
COPY STRUCTURE TO temp\c&xtable
SELECT 0
USE temp\c&xtable excl

FUNCTION zipencrypt (xtext)
	gmemo1 = xtext
	gmemo2 = ''
	odynazip.ActionDZ = 6
RETURN gmemo2

FUNCTION unzipencrypt (xtext)
	gmemo2 = xtext
	gmemo1 = ''
	odynaUnzip.ActionDZ = 10
RETURN gmemo1

FUNCTION zipdoc (xfile)
	odynaitemlist = odynazip.itemlist
	odynazipfile = odynazip.zipfile
	odynarecurseflag = odynazip.recurseflag
	odynaencryptflag = odynazip.encryptflag

	odynazip.encryptflag = .t.
	odynazip.itemlist = '"'+xfile+'"'
	odynazip.zipfile = JUSTPATH(xfile)+'\'+JUSTstem(xfile)+'.zip'
	odynazip.recurseflag = .t.
	odynazip.ActionDZ = 4

	odynazip.itemlist = odynaitemlist
	odynazip.zipfile = odynazipfile
	odynazip.recurseflag = odynarecurseflag
	odynazip.encryptflag = odynaencryptflag

RETURN 

FUNCTION unzipdoc (xfile)

	odynadestination = odynaUnzip.destination 
	oDynarecurseflag = oDynaUnzip.recurseflag
	oDynadecryptFlag = oDynaUnzip.decryptFlag
	odynatestflag = odynaUnzip.testflag
	odynazipfile = odynaunzip.zipfile
	odynafilespec = odynaunzip.filespec
	odynaoverwriteflag = odynaunzip.overwriteflag

	odynaUnzip.destination = SYS(5)+SYS(2003)
	oDynaUnzip.recurseflag = .t.
	oDynaUnzip.decryptFlag = .t.
	odynaUnzip.testflag = .f.
	odynaunzip.zipfile = xfile
	odynaunzip.filespec = '*.*'
	odynaunzip.overwriteflag = .t.
	odynaUnzip.ActionDZ = 8

	odynaUnzip.destination = odynadestination 
	oDynaUnzip.recurseflag = oDynarecurseflag
	oDynaUnzip.decryptFlag = oDynadecryptFlag
	odynaUnzip.testflag = odynatestflag
	odynaunzip.zipfile = odynazipfile
	odynaunzip.filespec = odynafilespec
	odynaunzip.overwriteflag = odynaoverwriteflag
	
RETURN


PUBLIC MainZIPForm as Form
PUBLIC MainUnZIPForm as Form

DEFINE CLASS DzHandler AS Custom

	IMPLEMENTS _dzactxEvents IN {0E9D0E41-7AB8-11D1-9400-00A0248F2EF0}#1.0

	PROCEDURE _dzactxEvents_ZipMajorStatus(ItemName As String, Percent As Numeric, Cancel As Numeric) As None
	*	
	MainZIPForm.MajorStatus.StatusText = ItemName
	MainZIPForm.MajorStatus.StatusPercent = Percent
	ENDPROC

	PROCEDURE _dzactxEvents_ZipMemToMemCallback(lAction As Numeric, lpMemBuf As String, pdwSize As Numeric, dwTotalReadL As Numeric, dwTotalReadH As Numeric, dwTotalWrittenL As Numeric, dwTotalWrittenH As Numeric, plRet As Numeric) As None 
	*
		*** ActiveX Control Event ***
*		LPARAMETERS laction, lpmembuf, pdwsize, dwtotalreadl, dwtotalreadh, dwtotalwrittenl, dwtotalwrittenh, plret

		if laction=0		&& MEM_READ_DATA
			lpmembuf = SUBSTR(gmemo1, dwtotalreadl + 1, pdwsize)
		    If (UseEncodedBinary = .T.) Then
		      && Convert the string into ASCII Hex
		      lpmembuf = EncodeBinaryString(lpmembuf)
		      && There are two characters per each data byte in the encoded string
		      && we need to tell DynaZIP how many actual Data Bytes we are transferring
		      pdwSize = Len(lpmembuf) / 2
		    Else
		      pdwSize = Len(lpmembuf)
		    EndIf
			if ((pdwsize + dwtotalreadl) >= LEN(gmemo1))
				plret=0		&& MEM_DONE
			else
				plret=3		&& MEM_CONTINUE
			endif
		else
			if laction=1	&& MEM_WRITE_DATA
			  if(dwtotalwrittenl = 0) then
			    && if first write then pre-clear the memo field
			    gmemo2 = ''
			  endif
		      * cms=CompressedMemoString
			  cms = gmemo2
		      If UseEncodedBinary = .T. Then
		        && Note pdwSize contains the number of Binary Data bytes being transferred.
		        && There are actually two characters per each data byte in the encoded string
		        && set by DynaZIP
		        newdata = Left(lpmembuf, pdwsize * 2)
		        cms = cms + DecodeBinaryString(newdata)
		      Else
		        cms = cms + Left(lpmembuf, pdwSize)
		      EndIf
		      plRet = 3
			  gmemo2 = cms
			else
				plret = 1		&& MEM_ERROR
			endif
		endif
	ENDPROC

	PROCEDURE _dzactxEvents_ZipMessageCallback(MsgID As Numeric, mbType As Numeric, p1 As Numeric, p2 As Numeric, sz1 As String, sz2 As String, rc As Numeric) As None
	*
	DO CASE
	  CASE MsgID <> 13
	    sz2 = "My ZIP Message Display"
	  OTHERWISE
	  ENDCASE
	ENDPROC

	PROCEDURE _dzactxEvents_ZipMinorStatus(ItemName As String, Percent As Numeric, Cancel As Numeric) As None
	*
	MainZIPForm.MinorStatus.StatusText = ItemName
	MainZIPForm.MinorStatus.StatusPercent = Percent
	ENDPROC

	PROCEDURE _dzactxEvents_ZipRenameCallback(ItemName As String, iDate As Numeric, iTime As Numeric, lAttrib As Numeric, OrigItemName As String, rc As Numeric) As None
	*
	ENDPROC

	PROCEDURE _dzactxEvents_ZipRenameCallbackEX(ItemName As String, iDate As Numeric, iTime As Numeric, lAttrib As Numeric, OrigItemName As String, lorigsize as numeric, lorigsizeh as numeric, rc As Numeric) As None
	*
	ENDPROC

	PROCEDURE _dzactxEvents_ZipMemToMemCallback1(lAction As Numeric, lpMemBuf As String, pdwSize As Numeric, dwTotalReadL As Numeric, dwTotalReadH As Numeric, dwTotalWrittenL As Numeric, dwTotalWrittenH As Numeric, plRet As Numeric) As None 
	*
	ENDPROC
ENDDEFINE
DEFINE CLASS DuzHandler AS Custom

	IMPLEMENTS _duzactxEvents IN {0FB90DC1-97D1-11D1-87C0-444553540000}#1.0

	PROCEDURE _duzactxEvents_UnZipMajorStatus(ItemName As String, Percent As Numeric, Cancel As Numeric) As None
	*	
	MainUnZIPForm.MajorStatus.StatusText = ItemName
	MainUnZIPForm.MajorStatus.StatusPercent = Percent
	ENDPROC

	PROCEDURE _duzactxEvents_UnZipMemToMemCallback(lAction As Numeric, lpMemBuf As String, pdwSize As Numeric, dwTotalReadL As Numeric, dwTotalReadH As Numeric, dwTotalWrittenL As Numeric, dwTotalWrittenH As Numeric, plRet As Numeric) As None 
	*
		*** ActiveX Control Event ***
*		LPARAMETERS laction, lpmembuf, pdwsize, dwtotalreadl, dwtotalreadh, dwtotalwrittenl, dwtotalwrittenh, plret

		IF lAction=0			&& MEM_READ_DATA
		    lpmembuf = SUBSTR(gmemo2, dwTotalReadL + 1, pdwSize)
		    If UseEncodedBinary = .T. Then
		      && An encoded string contains two characters for each data byte transferred
		      lpmembuf = EncodeBinaryString(lpmembuf)
		      && DynaZIP expects the actual number of Binary Data bytes transferred
		      pdwSize = Len(lpmembuf) / 2
		    Else
		      pdwSize = Len(lpMemBuf)
		    EndIf
		    IF ((pdwSize + dwTotalReadL) >= LEN(gmemo2))
		      	plRet=0			&& MEM_DONE
		    ELSE
		      	plRet=3			&& MEM_CONTINUE
		    ENDIF
		ELSE
			IF lAction=1		&& MEM_WRITE_DATA
			  if(dwtotalwrittenl = 0) then
			    && if starting to write then initialize the memo field to empty
			    gmemo1 = ""
			  endif
			  * ucms=UnCompressedMemoString
		      ucms = gmemo1
		      If UseEncodedBinary = .T. Then
		        && An encoded string contains two characters for each data byte transferred
		        && DynaZIP sets pdwSize to the number of Binary Data Byte actually transferred
		        newdata = Left(lpmembuf, pdwsize * 2)
		        ucms = ucms + DecodeBinaryString(newdata)
		      Else
		       ucms = ucms + Left(lpMemBuf, pdwSize)
		      EndIf
		      plRet = 3 		&& MEM_CONTINUE
		      gmemo1 = ucms
			ELSE
			    plRet=1			&& MEM_ERROR
			ENDIF
		ENDIF

	ENDPROC

	PROCEDURE _duzactxEvents_UnZipMessageCallback(MsgID As Numeric, mbType As Numeric, p1 As Numeric, p2 As Numeric, sz1 As String, sz2 As String, rc As Numeric) As None
	*
	DO CASE
	  CASE MsgID <> 13
	    sz2 = "My UnZIP Message Display"
	  OTHERWISE
	  ENDCASE
	ENDPROC

	PROCEDURE _duzactxEvents_UnZipMinorStatus(ItemName As String, Percent As Numeric, Cancel As Numeric) As None
	*
	MainUnZIPForm.MinorStatus.StatusText = ItemName
	MainUnZIPForm.MinorStatus.StatusPercent = Percent
	ENDPROC

	PROCEDURE _duzactxEvents_UnZipRenameCallback(ItemName As String, iDate As Numeric, iTime As Numeric, lAttrib As Numeric, OrigItemName As String, rc As Numeric) As None
	*
	ENDPROC

	PROCEDURE _duzactxEvents_UnZipRenameCallbackEX(ItemName As String, iDate As Numeric, iTime As Numeric, lAttrib As Numeric, OrigItemName As String, lorigsize as numeric, lorigsizeh as numeric, rc As Numeric) As None
	*
	ENDPROC

	PROCEDURE _duzactxEvents_UnZipMemToMemCallback1(lAction As Numeric, lpMemBuf As String, pdwSize As Numeric, dwTotalReadL As Numeric, dwTotalReadH As Numeric, dwTotalWrittenL As Numeric, dwTotalWrittenH As Numeric, plRet As Numeric) As None 
	*
	ENDPROC
ENDDEFINE

* Converts an ASCII HEX string into a binary string
FUNCTION DecodeBinaryString
PARAMETERS str1

  hex9 = Asc("9")
  hexA = Asc("A")
  hex0 = Asc("0")
  i = 1
  j = 1
  l1 = Len(str1)
  s = ""
  do While i < l1
    * An encoded BYTE is formaed by two ASCII Hex values
    * for example "56" means the MS part is 5 and the LS part is 6
    * Get MS part of this Encoded Character
    c2 = Asc(SUBSTR(str1, i, 1))
    * Decode the MS part
    If (c2 > hex9) Then
      c2 = c2 - hexA + 10
    Else
      c2 = c2 - hex0
    EndIf
    * Get LS part of this Encoded Character
    i = i + 1
    c1 = Asc(SUBSTR(str1, i, 1))
    && Decode the LS part
    If (c1 > hex9) Then
      c1 = c1 - hexA + 10
    Else
      c1 = c1 - hex0
    EndIf
    * Put the Decoded MS and LS parts together into one BINARY value
    i = i + 1
    s = s + Chr((c2 * 16) + c1)
    j = j + 1
  enddo
return(s)

* Converts a binary data string into ASCII HEX values needed by DynaZIP
FUNCTION EncodeBinaryString
PARAMETERS str1

  * Load the ACII HEX translations into the lookup string
  slookup = "000102030405060708090A0B0C0D0E0F"
  slookup = slookup + "101112131415161718191A1B1C1D1E1F"
  slookup = slookup + "202122232425262728292A2B2C2D2E2F"
  slookup = slookup + "303132333435363738393A3B3C3D3E3F"
  slookup = slookup + "404142434445464748494A4B4C4D4E4F"
  slookup = slookup + "505152535455565758595A5B5C5D5E5F"
  slookup = slookup + "606162636465666768696A6B6C6D6E6F"
  slookup = slookup + "707172737475767778797A7B7C7D7E7F"
  slookup = slookup + "808182838485868788898A8B8C8D8E8F"
  slookup = slookup + "909192939495969798999A9B9C9D9E9F"
  slookup = slookup + "A0A1A2A3A4A5A6A7A8A9AAABACADAEAF"
  slookup = slookup + "B0B1B2B3B4B5B6B7B8B9BABBBCBDBEBF"
  slookup = slookup + "C0C1C2C3C4C5C6C7C8C9CACBCCCDCECF"
  slookup = slookup + "D0D1D2D3D4D5D6D7D8D9DADBDCDDDEDF"
  slookup = slookup + "E0E1E2E3E4E5E6E7E8E9EAEBECEDEEEF"
  slookup = slookup + "F0F1F2F3F4F5F6F7F8F9FAFBFCFDFEFF"
  
  i = 1
  j = 1
  l1 = Len(str1)
  s = ""
  Do While i <= l1
    * get next BYTE
    c1 = 2 * Asc(SUBSTR(str1, i, 1))
    * Lookup Ascii Hex equivalent and append to string
    s = s + SUBSTR(slookup, c1 + 1, 2)
    j = j + 2
    i = i + 1
  Enddo
return(s)

FUNCTION getnumericversion (xversion)
	IF EMPTY(xversion)
		RETURN -1
	endif
	IF AT('.',xversion)!=0
		x100k = VAL(SUBSTR(xversion,1,AT('.',xversion)))*100000
	ELSE
		x100k = 0
	ENDIF
	IF AT('.',xversion,2)!=0
		x10k = VAL(SUBSTR(xversion,AT('.',xversion)+1,(AT('.',xversion,2)-AT('.',xversion)-1)))*10000
	ELSE
		x10k = 0
	ENDIF
	IF AT('.',xversion,2)!=0
		x1k = VAL(SUBSTR(xversion+'0000',AT('.',xversion,2)+1,4))
	ELSE
		x1k = 0
	ENDIF
RETURN x100k+x10k+x1k


FUNCTION DENCRYPT
*****************************************************************************
*                                DENCRYPT                                    *
*****************************************************************************
PARAMETER dcrypt
retvalue=""
dcrypt=trim(dcrypt)
upto=LEN(dcrypt)
TIMES=INT(UPTO/32)+1
tot=1
DO WHILE tot<=upto
	txvar=ASC(SUBSTR(dcrypt,tot,1))-asc(substr(REPL("FIDJEUNAKJFHASKSLCMFRHAKDKDFJFJM",TIMES),tot,1))
	if txvar<0
		txvar=256+txvar
	endif
   retvalue=retvalue+CHR(txvar)
   tot=tot+1
ENDDO
RETURN retvalue
*
*****************************************************************************
*

FUNCTION accessdecrypt

PARAMETERS str1


  hex9 = Asc("9")
  hexA = Asc("A")
  hex0 = Asc("0")
  i = 1
  j = 1
  l1 = Len(str1)
	TIMES=INT(l1/32)+1

  s = ""
  do While i < l1
    * An encoded BYTE is formaed by two ASCII Hex values
    * for example "56" means the MS part is 5 and the LS part is 6
    * Get MS part of this Encoded Character
    c2 = Asc(SUBSTR(str1, i, 1))
    * Decode the MS part
    If (c2 > hex9) Then
      c2 = c2 - hexA + 10
    Else
      c2 = c2 - hex0
    EndIf
    * Get LS part of this Encoded Character
    i = i + 1
    c1 = Asc(SUBSTR(str1, i, 1))
    && Decode the LS part
    If (c1 > hex9) Then
      c1 = c1 - hexA + 10
    Else
      c1 = c1 - hex0
    EndIf
    * Put the Decoded MS and LS parts together into one BINARY value
    s = s + Chr(((c2 * 16) + c1) - asc(substr(REPL("FIDJEUNAKJFHASKSLCMFRHAKDKDFJFJM",TIMES),j,1)))
    i = i + 1
    j = j + 1
  enddo
return(s)

FUNCTION toString
PARAMETERS lValue

IF TYPE("lValue")=="U" OR EMPTY(lValue)
	RETURN ""
ENDIF

xType = TYPE("lValue")
DO case
CASE xType=="C"
	RETURN lValue
CASE xType=="D"
	RETURN DTOC(lValue)
CASE xType=="T"
	RETURN TTOC(lValue)
CASE xType=="N"
	RETURN ALLTRIM(STR(lValue))
CASE xType=="L"
	RETURN IIF(lValue,"T","F")
OTHERWISE
	RETURN ""
ENDCASE