*****************************************************************************************************
FUNCTION dosqlselect
PARAMETERS xsqlselectstring, xsqlfrom, xsqlintotype, xsqlinto, xsqlwhere, xsqlgroup, xsqlorder, xsqlhaving
LOCAL xsql, lolderror, lerror, xtally, xcfile
xsqlintotype = UPPER(xsqlintotype)
IF EMPTY(xsqlselectstring) OR EMPTY(xsqlfrom) OR EMPTY(xsqlintotype) OR EMPTY(xsqlinto)
	*NO STRING, SOURCE TABLE OR TARGET DETAILS
	RETURN -10
ENDIF
IF !UPPER(xsqlintotype)$'CURSOR TCURSOR TABLE ARRAY STRUCTURE MEMVAR APPTABLE'
	*INVALID TARGET TYPE
	RETURN -11
endif

DO case
CASE xsqlintotype = 'CURSOR'
	xsql = xsqlselectstring+' from '+xsqlfrom+ ' into cursor '+xsqlinto
CASE xsqlintotype = 'ARRAY'
	xsql = xsqlselectstring+' from '+xsqlfrom+ ' into array '+xsqlinto
CASE xsqlintotype = 'TABLE'
	xsql = xsqlselectstring+' from '+xsqlfrom+ ' into cursor sqlresults'
	SELECT (xsqlinto)
	ZAP
CASE xsqlintotype = 'APPTABLE'
	xsql = xsqlselectstring+' from '+xsqlfrom+ ' into cursor sqlresults'
CASE xsqlintotype = 'TCURSOR'
	xsql = xsqlselectstring+' from '+xsqlfrom+ ' into cursor sqlresults'
CASE xsqlintotype = 'STRUCTURE'
	xsql = xsqlselectstring+' from '+xsqlfrom+ ' into cursor sqlresults'
	xsqlwhere = '.f.'
CASE xsqlintotype = 'MEMVAR'
	xsql = xsqlselectstring+' from '+xsqlfrom+ ' into cursor sqlresults'
ENDCASE
IF !EMPTY(xsqlwhere)
	xsql = xsql + ' where '+xsqlwhere
endif
IF !EMPTY(xsqlgroup)
	xsql = xsql + ' group by '+xsqlgroup
endif
IF !EMPTY(xsqlorder)
	xsql = xsql + ' order by '+xsqlorder
endif
IF !EMPTY(xsqlhaving)
	xsql = xsql + ' having '+xsqlhaving
ENDIF

xleaveopen = .f.
IF !','$xsqlfrom
	IF USED(xsqlfrom)
		xleaveopen = .t.
	endif
endif

lolderror = ON('error')
nerror = -1
ON ERROR nerror = ERROR()
SELECT &xsql
IF nerror!=-1
	updateerrorlog('SQL Select Error #'+ALLTRIM(STR(nerror))+'. Will retry.')
	LOCAL xtimes
	xtimes = 0
	DO WHILE xtimes<=10
		nerror = -1
		IF DIRECTORY(m.currentdatafolder+m.currentdatabase)
			SELECT &xsql
		ELSE
			nerror = -2
		endif
		IF nerror = -1
			updateerrorlog('SQL Select Succeeded after attempt #'+ALLTRIM(STR(xtimes)))
			EXIT
		endif
		INKEY(.5)
		xtimes = xtimes + 1 
		updateerrorlog('SQL Select Error #'+ALLTRIM(STR(nerror))+'. Retry #'+ALLTRIM(STR(xtimes)))
	ENDDO
	IF nerror!=-1
		ON ERROR &lolderror
		SELECT &xsql
	endif
endif
ON ERROR &lolderror
xtally = _tally
IF lerror
	RETURN -1
ENDIF

xcfile = DBF()
IF !xleaveopen
	IF ','$xsqlfrom
		xsqlfrom = xsqlfrom+', '
		DO WHILE ','$xsqlfrom AND !EMPTY(xsqlfrom)
			xthisfrom = SUBSTR(xsqlfrom,1,AT(',',xsqlfrom)-1)
			IF USED(xthisfrom)
				USE IN &xthisfrom
			ENDIF
			xsqlfrom = SUBSTR(xsqlfrom,AT(',',xsqlfrom)+1)
		enddo
	else
		IF USED(xsqlfrom)
			USE IN &xsqlfrom
		endif
	endif
endif
DO case
case xsqlintotype = 'TABLE' OR xsqlintotype = 'APPTABLE'
	SELECT (xsqlinto)
	IF xtally > 0
		APPEND FROM (xcfile)
	endif
	USE IN sqlresults
CASE xsqlintotype = 'TCURSOR'
	AFIELDS(asqlresults,'sqlresults')
	CREATE CURSOR &xsqlinto FROM ARRAY asqlresults
	IF xtally > 0
		APPEND FROM (xcfile)
	endif
	USE IN sqlresults
CASE xsqlintotype = 'STRUCTURE'
	SELECT sqlresults
	COPY STRUCTURE TO &xsqlinto
	USE IN sqlresults
CASE xsqlintotype = 'MEMVAR'
	SELECT sqlresults
	GO top
	SCATTER MEMVAR memo
	USE IN sqlresults
ENDCASE
RETURN xtally

*****************************************************************************************************
FUNCTION dosqlupdate
PARAMETERS xtable, xaupdates, xindexfield, xindexkey, xwhere, xskipdt
LOCAL xsql
IF EMPTY(xtable)
	*NO TABLE
	RETURN -20
ENDIF
xtable = UPPER(xtable)
IF EMPTY(xaupdates)
	*NO fields string
	RETURN -21
endif
DO case 
CASE empty(xwhere) AND EMPTY(xindexfield) AND EMPTY(xindexkey)
	*NO condition
	RETURN -23
CASE EMPTY(xwhere) AND !EMPTY(xindexfield) AND !EMPTY(xindexkey)
	xwhere = xindexfield+' = "'+xindexkey+'"'
CASE !EMPTY(xwhere) AND !EMPTY(xindexfield) AND !EMPTY(xindexkey)
	xwhere = xwhere + ' and '+xindexfield+' = "'+xindexkey+'"'
endcase
IF EMPTY(xwhere)
	RETURN -24
ENDIF

LOCAL xloop
xset = ''
FOR xloop = 1 TO ALEN(xaupdates,1)
	IF !EMPTY(xset)
		xset = xset + ','
	ENDIF
	xset = xset + xaupdates(xloop,1)+' = xaupdates('+ALLTRIM(STR(xloop))+',2)'
next
IF !xskipdt
	xset = xset + ',moddate=DATE(), modtime = TIME(), moduser = uinitials'
ENDIF

xsql = '"'+xtable + '" set ' + xset + ' where '+xwhere

IF !USED(xtable)
	IF !opendbf(xtable)
		RETURN -25
	endif
endif
lolderror = ON('error')
lerror = .f.
ON ERROR lerror = .t.
UPDATE &xsql
ON ERROR &lolderror
IF USED(xtable)
	USE IN &xtable
endif

IF lerror
	RETURN -1
ELSE
	RETURN _tally
ENDIF

*****************************************************************************************************
FUNCTION dosqlinsert
PARAMETERS xtable, xaupdates, xindexfield, xindexkey, xskipdt
LOCAL xsql,xfields,xvalues
IF EMPTY(xtable)
	*NO TABLE
	RETURN -30
ENDIF
IF EMPTY(xaupdates)
	*NO field list
	RETURN -31
endif
IF EMPTY(xindexfield)
	xindexfield = ''
ENDIF
IF EMPTY(xindexkey)
	xindexkey = ''
ENDIF

LOCAL xloop
xfields = ''
xvalues = ''
FOR xloop = 1 TO ALEN(xaupdates,1)
	IF !EMPTY(xfields)
		xfields = xfields + ','
	ENDIF
	xfields = xfields + xaupdates(xloop,1)
	IF !EMPTY(xvalues)
		xvalues = xvalues + ','
	ENDIF
	xvalues = xvalues + 'xaupdates('+ALLTRIM(STR(xloop))+',2)'
next
IF !EMPTY(xindexfield)
	xfields = xfields+','+xindexfield
	xvalues = xvalues+',"'+xindexkey+'"'
ENDIF
IF !xskipdt
	xfields = xfields + ',adddate,addtime,adduser'
	xvalues = xvalues + ',DATE(),TIME(),uinitials'
endif

xsql = '"'+xtable + '" ('+xfields +') values ('+xvalues+')'

lolderror = ON('error')
lerror = .f.
ON ERROR lerror = .t.
INSERT INTO &xsql
ON ERROR &lolderror
IF ','$xtable
	xtable = xtable+', '
	DO WHILE ','xtable AND !EMPTY(xtable)
		xthisfrom = SUBSTR(xtable,1,AT(',',xtable)-1)
		IF USED(xthisfrom)
			USE IN &xthisfrom
		ENDIF
		xtable= SUBSTR(xtable,AT(',',xtable)+1)
	enddo
else
	IF USED(xtable)
		USE IN &xtable
	endif
endif
IF lerror
	RETURN -1
ELSE
	RETURN 1
ENDIF

*****************************************************************************************************
FUNCTION dosqldelete
PARAMETERS xtable, xindexfield,xindexkey, xwhere
LOCAL xsql
IF EMPTY(xtable)
	*NO TABLE
	RETURN -40
ENDIF
DO case empty(xwhere) AND EMPTY(xindexfield) AND EMPTY(xindexkey)
	*NO condition
	RETURN -23
CASE EMPTY(xwhere) AND EMPTY(xindexkey)
	RETURN -42
CASE EMPTY(xwhere) AND !EMPTY(xindexfield) AND !EMPTY(xindexkey)
	xwhere = xindexfield+' = "'+xindexkey+'"'
CASE !EMPTY(xwhere) AND !EMPTY(xindexfield) AND !EMPTY(xindexkey)
	xwhere = xwhere + ' and '+xindexfield+' = "'+xindexkey+'"'
endcase
IF EMPTY(xwhere)
	*NO field list
	RETURN -41
endif

xsql = '"'+xtable + '" where '+xwhere

lolderror = ON('error')
lerror = .f.
ON ERROR lerror = .t.
DELETE from &xsql
ON ERROR &lolderror
IF ','$xtable
	xtable = xtable+', '
	DO WHILE ','xtable AND !EMPTY(xtable)
		xthisfrom = SUBSTR(xtable,1,AT(',',xtable)-1)
		IF USED(xthisfrom)
			USE IN &xthisfrom
		ENDIF
		xtable= SUBSTR(xtable,AT(',',xtable)+1)
	enddo
else
	IF USED(xtable)
		USE IN &xtable
	endif
endif
IF lerror
	RETURN -1
ELSE
	RETURN _tally
ENDIF

*****************************************************************************************************
FUNCTION dosqlupdate2
PARAMETERS xtable, xaupdates, xindexfield, xwhere, xskipdt
LOCAL xcount, xpercbar
xcount = 0
xretval = dosqlselect(xindexfield,xtable,'CURSOR','sqltemp',xwhere)
SELECT sqltemp
xpercbar = .f.
IF RECCOUNT()>10
	actiperc('Please Wait, Updating Records')
	xpercbar = .t.
endif
SCAN
	IF xpercbar
		updperc(RECNO()/RECCOUNT()*100)
	endif
	retval = dosqlupdate(xtable,@xaupdates,xindexfield,sqltemp.&xindexfield,.f.,xskipdt)
	IF retval > 0
		xcount = xcount + retval
	endif
	SELECT sqltemp
ENDSCAN
IF xpercbar
	deactiperc()
endif
SELECT sqltemp
USE
RETURN xcount

*****************************************************************************************************
FUNCTION dosqlinsert2
PARAMETERS xtable, xtable2,xindexfield, xwhere,xskipdt
IF EMPTY(xwhere)
	xwhere = '.T.'
endif
LOCAL xcount,xcount1, xpercbar
xcount1 = 0
xcount = 0
SELECT (xtable2)
xpercbar = .f.
IF RECCOUNT()>10
	actiperc('Please Wait, Updating Records')
	xpercbar = .t.
endif
SCAN FOR &xwhere
	IF xpercbar
		updperc(RECNO()/RECCOUNT()*100)
	endif
	DIMENSION xaupdates(1,2)
	xaupdates(1,1) = ''
	xaupdates(1,2) = ''
	FOR LOOP = 1 TO FCOUNT()
		mv = LOWER(FIELD(loop))
		IF !(mv $ 'adddate, addtime, adduser, moddate, modtime, moduser') AND mv!=xindexfield
			IF !EMPTY(&xtable2..&mv)
				IF !EMPTY(xaupdates(1,1))
					DIMENSION xaupdates(ALEN(xaupdates,1)+1,2)
				ENDIF
				xaupdates(ALEN(xaupdates,1),1) = mv
				xaupdates(ALEN(xaupdates,1),2) = &xtable2..&mv
			ENDIF
		endif
	next				
	replace &xtable2..&xindexfield WITH uniqueid()
	xcount1 = dosqlinsert(xtable,@xaupdates,xindexfield,&xtable2..&xindexfield,xskipdt)
	IF xcount1>1
		xcount = xcount + xcount1
	endif
ENDSCAN
IF xpercbar
	deactiperc()
endif

*****************************************************************************************************
FUNCTION dosqldelete2
PARAMETERS xtable, xindexfield, xwhere
LOCAL xcount,xpercbar
xcount = 0
xretval = dosqlselect(xindexfield,xtable,'CURSOR','sqltemp',xwhere)
SELECT sqltemp
xpercbar = .f.
IF RECCOUNT()>10
	actiperc('Please Wait, Updating Records')
	xpercbar = .t.
endif
SCAN
	IF xpercbar
		updperc(RECNO()/RECCOUNT()*100)
	endif
	retval = dosqldelete(xtable,xindexfield,sqltemp.&xindexfield)
	IF retval > 0
		xcount = xcount + retval
	endif
	SELECT sqltemp
ENDSCAN
IF xpercbar
	deactiperc()
endif
SELECT sqltemp
USE
RETURN xcount

*****************************************************************************************************
FUNCTION dosqlconnect
PARAMETERS xtype, CurrentDataFolder,CurrentDatabase
DO case
CASE xtype = 'VFP'
	*	*	*	*	*	*	*	*	*	*	*	*	*	*	*	*
	xstring = m.CurrentDataFolder+m.CurrentDatabase+'data;'
	SET PATH TO &xstring

	*create local cursor files
	tempdir = SYS(5)+sys(2003)

	IF !DIRECTORY('output')
		MD output
	endif
	IF !DIRECTORY('temp')
		MD temp
	endif
	*temp charges cursor
	createcursor('charges','ccharges')
	*temp invoice cursor
	createcursor('invoice','cinvoice')
	*temp activity cursor
	createcursor('activity','cactivity')
	*temp client cursor
	createcursor('client','cclient')
	*
	*temp client contact cursor
	createclcontact()
	LOCAL xclcontact
	xclcontact = oapp.clcontact
	SELECT 0
	USE &xclcontact EXCLUSIVE ALIAS clcontact
	INDEX on contlistid TAG contlistid
	INDEX ON contactid TAG contactid
	INDEX on indexkey TAG indexkey
	USE
	*
	*temp appointm cursor
	createcursor('appointm','cappointm')
	*temp diary cursor
	createcursor('appointm','cdiary')

	LOCAL xcdiary
	xcdiary = oapp.cdiary
	SELECT 0
	USE "&xcdiary" EXCLUSIVE ALIAS cdiary
	INDEX ON diaryuser+STR(datetimlog,12)+STR(duration,4)+haptindex+STR(200-ASC(apttype),3) tag chdatetiml
	INDEX ON room+STR(datetimlog,12)+STR(duration,4)+haptindex+STR(200-ASC(apttype),3) tag rmdattiml
	INDEX ON aptindex TAG aptindex
	INDEX ON rptindex TAG rptindex
	USE

	*temp diarweek cursor
	createcursor('diarweek','cdiarwk')
	LOCAL xcdiarwk
	xcdiarwk = oapp.cdiarwk
	SELECT 0
	USE "&xcdiarwk" EXCLUSIVE ALIAS cdiarwk
	INDEX ON DIARYUSER + DTOS(WCOMMDATE) + DAY + STARTTIME tag day
	INDEX ON DIARYUSER + DTOS(WCOMMDATE) + DAY + STARTTIME DESC tag day_d
	INDEX ON room + DTOS(WCOMMDATE) + DAY + STARTTIME TAG room
	INDEX ON room + DTOS(WCOMMDATE) + DAY + STARTTIME DESCENDING TAG room_d
	loaddiarweek()
	USE

	*temp holding list cursor
	createcursor('holding','cholding')
	LOCAL xcholding
	xcholding = oapp.cholding
	SELECT 0
	USE "&xcholding" EXCLUSIVE ALIAS cholding
	INDEX ON clientno TAG clientno
	INDEX ON holdindex TAG holdindex
	USE

	*temp consultation cursor
	createcursor('consultation','ccons')
	LOCAL xccons
	xccons = oapp.ccons
	SELECT 0
	USE "&xccons" EXCLUSIVE ALIAS ccons
	INDEX ON consultid TAG consultid
	USE

	*temp consultation forms cursor
	createcursor('forms','cform')
	LOCAL xcform
	xcform = oapp.cform
	SELECT 0
	USE "&xcform" EXCLUSIVE ALIAS cform
	INDEX ON consultid+formno+questionid TAG consultid
	INDEX ON consultid+formno+STR(qorder,5) TAG qorder
	USE

	*temp treatment cursor
	createcursor('treatment','ctreat')
	LOCAL xctreat
	xctreat = oapp.ctreat
	SELECT 0
	USE "&xctreat" EXCLUSIVE ALIAS ctreat
	INDEX ON treatid TAG treatid
	USE

	*temp annotations cursor
	createcursor('annotations','cannotate')
	LOCAL xcannotate
	xcannotate = oapp.cannotate
	SELECT 0
	USE "&xcannotate" EXCLUSIVE ALIAS cannotate
	INDEX ON questionid+annot_no TAG questionid
	INDEX on indexkey TAG indexkey
	USE

	*
	*load look up tables into arrays
	loadappttype()

	*in proclib so we can reload it after maintenance
	loadapptcfg()

	*in proclib to load after mainentance
	loadaclientcontact()

	*titles
	loadtitle()

	*source of intro
	loadintrosource()
	
RETURN .t.
endcase

************************************************************************************************
FUNCTION dosqldisconnect
*create local cursor files
tempdir = SYS(5)+sys(2003)

IF !EMPTY(oapp.cactivity)
	*temp activity cursor
	IF USED('cactivity')
		USE IN cactivity
	ENDIF
	xfile = oapp.cactivity+'.dbf'
	IF FILE(xfile)
		ERASE &xfile
	ENDIF
	xfile = oapp.cactivity+'.fpt'
	IF FILE(xfile)
		ERASE &xfile
	ENDIF
	xfile = oapp.cactivity+'.cdx'
	IF FILE(xfile)
		ERASE &xfile
	ENDIF
endif

IF !EMPTY(oapp.cclient)
	IF USED('cclient')
		USE IN cclient 
	ENDIF
	xfile = oapp.cclient +'.dbf'
	IF FILE(xfile)
		ERASE &xfile
	ENDIF
	xfile = oapp.cclient +'.fpt'
	IF FILE(xfile)
		ERASE &xfile
	ENDIF
	xfile = oapp.cclient +'.cdx'
	IF FILE(xfile)
		ERASE &xfile
	ENDIF
ENDIF
	
IF !EMPTY(oapp.cappointm)
	IF USED('cappointm')
		USE IN cappointm
	ENDIF
	xfile = oapp.cappointm+'.dbf'
	IF FILE(xfile)
		ERASE &xfile
	ENDIF
	xfile = oapp.cappointm+'.fpt'
	IF FILE(xfile)
		ERASE &xfile
	ENDIF
	xfile = oapp.cappointm+'.cdx'
	IF FILE(xfile)
		ERASE &xfile
	ENDIF
ENDIF

IF !EMPTY(oapp.ccons)
	IF USED('ccons')
		USE IN ccons
	ENDIF
	xfile = oapp.ccons+'.dbf'
	IF FILE(xfile)
		ERASE &xfile
	ENDIF
	xfile = oapp.ccons+'.fpt'
	IF FILE(xfile)
		ERASE &xfile
	ENDIF
	xfile = oapp.ccons+'.cdx'
	IF FILE(xfile)
		ERASE &xfile
	ENDIF
ENDIF

IF !EMPTY(oapp.cform)
	IF USED('cform')
		USE IN cform
	ENDIF
	xfile = oapp.cform+'.dbf'
	IF FILE(xfile)
		ERASE &xfile
	ENDIF
	xfile = oapp.cform+'.fpt'
	IF FILE(xfile)
		ERASE &xfile
	ENDIF
	xfile = oapp.cform+'.cdx'
	IF FILE(xfile)
		ERASE &xfile
	ENDIF
ENDIF

IF !EMPTY(oapp.ctreat)
	IF USED('ctreat')
		USE IN ctreat
	ENDIF
	xfile = oapp.ctreat+'.dbf'
	IF FILE(xfile)
		ERASE &xfile
	ENDIF
	xfile = oapp.ctreat+'.fpt'
	IF FILE(xfile)
		ERASE &xfile
	ENDIF
	xfile = oapp.ctreat+'.cdx'
	IF FILE(xfile)
		ERASE &xfile
	ENDIF
ENDIF

IF !EMPTY(oapp.cannotate)
	IF USED('cannotate')
		USE IN cannotate
	ENDIF
	xfile = oapp.cannotate+'.dbf'
	IF FILE(xfile)
		ERASE &xfile
	ENDIF
	xfile = oapp.cannotate+'.fpt'
	IF FILE(xfile)
		ERASE &xfile
	ENDIF
	xfile = oapp.cannotate+'.cdx'
	IF FILE(xfile)
		ERASE &xfile
	ENDIF
ENDIF

IF !EMPTY(oapp.cdiary)
	IF USED('cdiary')
		USE IN cdiary
	ENDIF
	xfile = oapp.cdiary+'.dbf'
	IF FILE(xfile)
		ERASE &xfile
	ENDIF
	xfile = oapp.cdiary+'.fpt'
	IF FILE(xfile)
		ERASE &xfile
	ENDIF
	xfile = oapp.cdiary+'.cdx'
	IF FILE(xfile)
		ERASE &xfile
	ENDIF
endif

IF !EMPTY(oapp.cdiarwk)
	IF USED('cdiarwk')
		USE IN cdiarwk
	ENDIF
	xfile = oapp.cdiarwk+'.dbf'
	IF FILE(xfile)
		ERASE &xfile
	ENDIF
	xfile = oapp.cdiarwk+'.fpt'
	IF FILE(xfile)
		ERASE &xfile
	ENDIF
	xfile = oapp.cdiarwk+'.cdx'
	IF FILE(xfile)
		ERASE &xfile
	ENDIF
ENDIF

IF !EMPTY(oapp.cholding)
	IF USED('cholding')
		USE IN cholding
	ENDIF
	xfile = oapp.cholding+'.dbf'
	IF FILE(xfile)
		ERASE &xfile
	ENDIF
	xfile = oapp.cholding+'.fpt'
	IF FILE(xfile)
		ERASE &xfile
	ENDIF
	xfile = oapp.cholding+'.cdx'
	IF FILE(xfile)
		ERASE &xfile
	ENDIF
endif

IF !EMPTY(oapp.clcontact)
	IF USED('clcontact')
		USE IN clcontact
	ENDIF
	xfile = oapp.clcontact+'.dbf'
	IF FILE(xfile)
		ERASE &xfile
	ENDIF
	xfile = oapp.clcontact+'.fpt'
	IF FILE(xfile)
		ERASE &xfile
	ENDIF
	xfile = oapp.clcontact+'.cdx'
	IF FILE(xfile)
		ERASE &xfile
	ENDIF
endif

IF !EMPTY(oapp.ccharges)
	IF USED('ccharges')
		USE IN ccharges
	ENDIF
	xfile = oapp.ccharges+'.dbf'
	IF FILE(xfile)
		ERASE &xfile
	ENDIF
	xfile = oapp.ccharges+'.fpt'
	IF FILE(xfile)
		ERASE &xfile
	ENDIF
	xfile = oapp.ccharges+'.cdx'
	IF FILE(xfile)
		ERASE &xfile
	ENDIF
endif

IF !EMPTY(oapp.cinvoice)
	IF USED('cinvoice')
		USE IN cinvoice
	ENDIF
	xfile = oapp.cinvoice+'.dbf'
	IF FILE(xfile)
		ERASE &xfile
	ENDIF
	xfile = oapp.cinvoice+'.fpt'
	IF FILE(xfile)
		ERASE &xfile
	ENDIF
	xfile = oapp.cinvoice+'.cdx'
	IF FILE(xfile)
		ERASE &xfile
	ENDIF
endif

*release the .lock file
IF oapp.lockfile>0
	FCLOSE(oapp.lockfile)
ENDIF
IF oapp.lockuser > 0 AND FILE(m.currentdatafolder+m.currentdatabase+'currentusers\user'+ALLTRIM(STR(oapp.lockuser))+'.lck')
	mv = m.currentdatafolder+m.currentdatabase+'currentusers\user'+ALLTRIM(STR(oapp.lockuser))+'.lck'
	IF FILE('&mv')
		ERASE &mv
	endif
	mv = m.currentdatafolder+m.currentdatabase+'currentusers\user'+ALLTRIM(STR(oapp.lockuser))+'.id'
	IF FILE('&mv')
		ERASE &mv
	endif
ENDIF

RETURN

***************************************

PROCEDURE createcursor
PARAMETERS xfile,xcursor
	*temp charges cursor
	tempdir = SYS(5)+sys(2003)
	IF USED(xcursor)
		USE IN &xcursor
	ENDIF
	set defa to temp
	oapp.&xcursor = 'temp\'+sys(3)
	set defa to "&tempdir"
	retval = dosqlselect('*',xfile,'STRUCTURE',oapp.&xcursor)
RETURN

PROCEDURE createclcontact
	tempdir = SYS(5)+sys(2003)
	IF USED('clcontact')
		USE IN clcontact
	ENDIF
	set defa to temp
	oapp.clcontact = 'temp\'+sys(3)
	set defa to "&tempdir"
	retval = dosqlselect('clientcontact.indexkey,clientcontact.contlistid,clientcontact.contactid,text,number,clientcontact.note,clientcontact.summary," " as status','clientcontact','STRUCTURE',oapp.clcontact)
RETURN

PROCEDURE createclhist
	tempdir = SYS(5)+sys(2003)
	set defa to temp
	oapp.clhist = 'temp\'+sys(3)
	set defa to "&tempdir"
	xfile = oapp.clhist
	select 0
	create cursor temp (clientno C(11), corder n(5))
	copy stru to (xfile)
	use
RETURN

PROCEDURE loadappttype
	retval = dosqlselect('*','appttype','ARRAY','aappttype','isactive','','appttype')
RETURN

procedure loadtitle
	DIMENSION atitle(1,2)
	retval = dosqlselect('title,sex','title','ARRAY','atitle','','','title')
	IF retval < 0
		DIMENSION atitle(1,2)
		atitle(1,1) = 'None'
		atitle(1,2) = ''
	ENDIF
RETURN

PROCEDURE loadintrosource
	DIMENSION aintrosource(1)
	retval = dosqlselect('introsourc','introsourc','ARRAY','aintrosource')
	IF retval < 0
		DIMENSION aintrosource(1)
		aintrosource(1,1) = 'None'
	ENDIF
return